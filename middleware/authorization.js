const tryCatch = require('../middleware/catchAssyncErrors')

const authorizationMiddleware = (type) => {
    const inputType = type  //store the input user access type
    return  tryCatch (async (req, res, next) => {
        const {userType} = req.user //get the userType from token 
        if (userType) { //if the user type exist in token then it will happen
            const isPermitted = inputType.includes(userType)
            if  (isPermitted) { //if is permitted  true then it will execute
                next ()
            }else {
                res.json ({
                    message: "Access denied"
                })
            }
        }else {
            res.json ({
                message: "Can not permitted to access because user Type not found"
            })
        }
    })
}

//export part 
module.exports = authorizationMiddleware