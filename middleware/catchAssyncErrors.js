module.exports = (theFucn) => (req, res, next) => {
    Promise.resolve(theFucn(req, res, next)).catch(next);
};

// exports.example = catchAssyncErrors(async (req, res, next) => {});
