1. project run command in development mode: npm run dev.
2. root directory is index.js

.Env File

1. PORT //set the server running port
2. MONGO_URL //set the mongodb data url
3. FILE_UPLOAD_LIMIT //set the maximum size of files for upload
4. JWT_SECRET // secret key for JWT authentication
5. JWT_EXPIRE // jwt expiration day ex: 5d
6. COOKIE_EXPIRE // cookie expiration day ex:5
