//import all module here
const express = require("express");
const app = express();
const socketio = require("socket.io");
const http = require("http");
const mongoose = require("mongoose");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const errMiddleware = require('./middleware/error')

// Handling uncaught Error exceptions
process.on("uncaughtException", (err) => {
    console.log(`Error: ${err.message}`);
    console.log(`Shutting down the server due to uncaught Error exceptions`);

    process.exit(1);
});

require("dotenv").config();

//get the dot env file
const PORT = process.env.PORT || 8080; //set the port number
const mongoUrl = process.env.MONGO_URL; //receive the mongodb url from dot env
const fileUploadLimit = process.env.FILE_UPLOAD_LIMIT; //get the limit of maximum size of file upload size in "mb"

//set the static folder
app.use(express.static("public"));
app.use(cookieParser());

//do the body parser and cors middleware part
app.use(
    express.json({
        limit: `${fileUploadLimit}mb`, //set the maximum size limit of upload file
    })
);
app.use(
    express.urlencoded({
        extended: true,
        limit: `${fileUploadLimit}mb`, //set the maximum size limit of upload file
    })
);
app.use(cors());

//create the server
const server = http.createServer(app);

//create the socket io server
const io = socketio(server);

//run the server
server.listen(PORT, () => console.log(`Server is running on Port ${PORT}`));

//connect to the data base
mongoose
    .connect(mongoUrl, {
        useUnifiedTopology: true,
        useNewUrlParser: true,
    })
    .then(() => {
        console.log(`Server is conntected to the data base`);
    })
    .catch((err) => console.error(err)); 

//root route
app.get("/", (req, res) => {
    res.send("<h1>Hello I am from root</h1>");
});

//all api route
app.use ("/address", require('./src/route/addressRouter') ) 
app.use ("/appointment", require('./src/route/appointmentRouter') )
app.use ("/balance", require('./src/route/balanceRouter') )  
app.use ("/campaign", require('./src/route/campaignRouter') ) 
app.use ("/category", require('./src/route/categoryRouter') ) 
app.use ("/cart", require('./src/route/cartRouter') ) 
// app.use ("/image", upload) 


//default route
app.get("*", (req, res) => {
    res.send(`<h1>Page not found</h1>`);
});

// unhandled promise rejection
process.on("unhandledRejection", (err) => {
    console.log(`Error: ${err.message} `);
    console.log(`Shutting down the server due to unhandled promise rejection`);
    server.close(() => {
        process.exit(1);
    });
});

app.use(errMiddleware)