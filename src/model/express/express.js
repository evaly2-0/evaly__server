const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const expressSchema = new Schema({
    slug: String,
    expressName: String,
    shop: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Shop'
        }
    ],
    banner: String,
    isDeleted: {
        type: Boolean,
        default: false
    },
    owner: {
        type: Schema.Types.ObjectId,
        ref: 'User' 
    }
}, { timestamps: true })

module.exports= mongoose.model('Express',expressSchema);