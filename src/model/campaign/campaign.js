const mongoose = require('mongoose')
const Schema = mongoose.Schema

const campaignSchema = new Schema({
    slug: String,
    officialInfo: {
        campaignName: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        shopInfo: [
            {
                type: mongoose.Types.ObjectId,
                ref: "Shop"
            }
        ],
        banner: String,
        termAndCondition : [String]
    },
    others: {
        isDeleted: {
            type: Boolean,
            default: false
        },
        isActive: {
            type: Boolean,
            default: true
        }
    }
},{
    timestamps: true
})

module.exports = mongoose.model ("Campaign", campaignSchema)