const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const addressSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    receipientName: String,
    phoneNumber: String,
    region: String,
    city: String,
    area: String,
    address: String,
    isPrimary: {
        type: Boolean,
        default: true
    },
    isDeleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true });

module.exports= mongoose.model('Address',addressSchema);