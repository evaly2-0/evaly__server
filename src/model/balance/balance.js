const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const balanceSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    mainBalance: Number,
    holdingBalance: Number,
    giftCardBalance: Number,
    cashBackBalance: Number
}, { timestamps: true })

module.exports= mongoose.model('Balance',balanceSchema);