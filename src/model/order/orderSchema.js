const mongoose = require('mongoose')
const Schema = mongoose.Schema

const orderSchema = new Schema ({
    slug: String,
    orderId: {
        type: String,
        required: true,
    },
    orderPlaceDate: {
        type: Date,
        default: Date.now
    },
    orderDetails: {
        orderItems: [
            {
                productDetails: {
                    type: mongoose.Types.Object,
                    required: true,
                    ref: "Product"
                },
                quantity: {
                    type: Number,
                    required: true,
                },
                price: {
                    type: Number,
                    required: true,
                }
            } //it will store the ordered product details
        ],
        totalPrice: {
            type: Number
        },
        totalPaid: {
            type: Number
        },
        dueAmount: {
            type: Number,
            default: 0
        }
    }, //set the order total item with price 
    incomeInfo: {
        sellerIncome: {
            type: Number,
            default: 0
        },
        officeIncome: {
            type: Number,
            default: 0
        }
    },
    orderTimeLine: {
        currentStatus: [
            {
                type: String,
                default: "pending",
                enum: ["pending", "confirmed", "processing", "picked", "shipped", "delivered", "cancel" ]
            }
        ],
        paymentStatus: {
            type: Boolean,
            default: false
        },
        isConfirmed: {
            type: Boolean,
            default: false
        },
        isCanceled: {
            type: Boolean,
            default: false
        }
    }, //it will set the order timeline status
    orderIssue: {
        type: mongoose.Types.ObjectId,
        ref: "Issue"
    },
    others : {
        orderedUser: {
            type: mongoose.Types.ObjectId,
            ref: "User",
            required: true
        },
        review: {
            type: mongoose.Types.ObjectId,
            ref: "Review"
        },
        shopInfo: {
            type: mongoose.Types.ObjectId,
            required: true,
            ref: "Shop"
        },
        isDeleted: {
            type: Boolean,
            default: false
        }
    }
},
{
    timestamps: true
})

module.exports = mongoose.model("Order", orderSchema)