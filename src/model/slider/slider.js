const mongoose = require('mongoose')
const Schema = mongoose.Schema

const sliderSchema = new Schema({
    slug: String,
    sliderInfo: {
        image: {
            type: String,
            required: true
        },
        priority: Number,
        pageLink: {
            type: String,
            required: true
        },
        sliderLocation: {
            type: String,
            enum: ["homePageOne", "homePageTwo", "campaign"],
            required: true
        }
    },
    others: {
        isDeleted: {
            type: Boolean,
            default: false
        },
        isActive: {
            type: Boolean,
            default: true
        }
    }
},{
    timestamps: true
})

//export part
module.exports = mongoose.model ("Slider", sliderSchema)