const {Schema, model}= require('mongoose')


const messageSchema = new Schema({
    message:String,
    sender:{ 
        type: Schema.Types.ObjectId, 
        ref:'User'
    },
    receiver:{ 
        type: Schema.Types.ObjectId, 
        ref:'User'
    },
    conversationId:{ 
        type: Schema.Types.ObjectId, 
        ref:'Conversation'
    },
    shop: {
        type: Schema.Types.ObjectId,
        ref: "Shop"
    },
    dateTime:{ type:Date, default:Date.now },
    isDeleted:{ type:Boolean, default:false }
    },
{
    timestamps:true
})
 
module.exports = model('Message', messageSchema)
