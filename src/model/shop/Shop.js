const {Schema, model}= require('mongoose')

const shopSchema = new Schema({
    slug: String,
    shopDetails:{
        shopName: String,
        shopId: String,
        shopCategory: String,
        shopType:{
            enum:["brand", "retail"]
        },
        owner:{ 
            type:Schema.Types.ObjectId,
            ref: 'User',
        },
    },
    sellingDetails: {
        totalOrder: {
            type: Number,
            default: 0
        },
        order: [{
            type: Schema.Types.ObjectId,
            ref: "Order"
        }],
        totalIncome: {
            type: Number,
            default: 0
        }
    },
    otherDetails:{
        shopContactNumber: String,
        tradeLicenceNumber: String,
        tradeLicenceImg: String,
        ownerNIDImg: String,
        bannerImg: String,
        shopAddress: String,
        
        shopType: {
            isExpress: { 
                type:Boolean, 
                default :false
            },
            isCampaign: { 
                type:Boolean, 
                default :false
            },
            cashOnDelivery:{ 
                type:Boolean, 
                default :false   
            },
        }
    },

    shopLocation:{ 
        division:String,
        district:String,
        country:String,
        latitude:Number,
        longitude:Number,
    },

    categoryOfProducts: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Category' // we need required schema
        }
    ],
    reviewDetails:{
        shopReview:[{ 
            type:Schema.Types.ObjectId, 
            ref: 'Review'
        }],
        totalReview: String, 
        averageReview: String,
    },

    follower:{ 
        type:Schema.Types.ObjectId,
        ref: 'Follower',
    },
    minimumOrderAmount:{ type:String, default:"" },
    campaign:{ 
        type:Schema.Types.ObjectId,
        ref: 'Campaign',
    },
    expressShop:{ 
        type:Schema.Types.ObjectId,
        ref: 'Express',
    },
    product:{ 
        type:Schema.Types.ObjectId,
        ref: 'Product',
    },

},{

    timestamps:true
})

module.exports = model('Shop', shopSchema)
