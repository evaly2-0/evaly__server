const mongoose = require('mongoose')
const Schema = mongoose.Schema

const commentSchema = new Schema({
    commentInfo: {
        comment: {
            type: String,
            required: true
        },
        commentBy: {
            type: mongoose.Types.ObjectId,
            ref: "User",
            required: true
        },
        replyPart : [
            {
                replyBy: {
                    type: mongoose.Types.ObjectId,
                    ref: "User"
                },
                replyComment: String,
                time: {
                    type: Date,
                    default: Date.now
                }
            }
        ]
    },
    others: {
        post: {
            type: mongoose.Types.ObjectId,
            ref: "Post", 
            required: true
        },
        isDeleted: {
            type: Boolean,
            default: false
        }
    }
},{
    timestamps: true
})

//export part 
module.exports = mongoose.model ("Comment", commentSchema)