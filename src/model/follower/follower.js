const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const folowerSchema = new Schema({
    followedBy: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    followingShops: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Shop'
        }
    ]
}, { timestamps: true })

module.exports = mongoose.model('Follower', folowerSchema);
