const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const cartSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    productDetails: [
        {
            quantity: Number,
            productInfo: {
                type: Schema.Types.ObjectId,
                ref: 'Product'
            }
        }
    ],
   bill:{
       shopWiseTotalAmmount: Number,
       subTotal: Number
   }
}, { timestamps: true });

module.exports= mongoose.model('Cart',cartSchema);