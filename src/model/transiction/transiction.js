const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const transictionSchema = new Schema({
    title: String,
    transictionType: {
        type: String,
        enum: ["self", "order"]
    },
    transictionDate: {
        type: Date,
        default:Date.now
    },
    amountAdd: Number,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    order: {
        type: Schema.Types.ObjectId,
        ref: "Order"
    }
}, { timestamps: true });

module.exports= mongoose.model('Transaction',transictionSchema);