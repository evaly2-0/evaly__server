const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const refundSchema = new Schema({
    refundAccountNumber: String,
    refundPlatform: String,
    OTP: {
        type: String
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    isDeleted:{
        type:Boolean,
        default: false
    },
    order: {
        type: Schema.Types.ObjectId,
        ref: "Order"
    }
}, { timestamps: true });

module.exports= mongoose.model('Refund',refundSchema);