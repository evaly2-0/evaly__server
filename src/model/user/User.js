const {Schema, model}= require('mongoose')
const bcrypt = require('bcrypt')

const userSchema = new Schema({
    slug: String,
    personalInfo:{
        firstName: String,
        lastName: String,
        transaction: [
            {
                type: Schema.Types.ObjectId,
                ref: 'Transaction' // we need required  schema
            }
        ],

        contactNumber:{ 
            type:String, 
            required : true , 
            unique : true 
        },
        contactEmail:{ 
            type:String, 
            required : true ,  
            unique : true 
        },
        Birthday: Date,

        Gender:{
            enum:["male", "female",'others'],
        },
        fatherName:String,
        motherName:String,
        fathersContactNumber:String,
        mothersContactNumber:String,

        otherEmail:[{
            type:String
        }],

        officialInfo:{
            occupation: String,
            organization:String,
            userType:{ 
                type:String, 
                enum:["user", "superAdmin","admin"] 
            },
            password: { 
                type:String,
                required : true 
            },
            isActive: { 
                type:Boolean, 
                default : false 
            },
            isDeleted: { 
                type:Boolean, 
                default: false 
            },
            epoint: { 
                type: Number, 
                default:  0
            }
        },
        following: {
            type: Schema.Types.ObjectId,
            ref: 'Following' // we need required  schema
        },
        shopCollection: {
            type: Schema.Types.ObjectId,
            ref: 'Shop' // we need required  schema
        },
        sellerIncomeDetails: {
            totalIncome: {
                type: Number, 
                default: 0
            },
            totalSell: [
                {
                    type: Schema.Types.ObjectId,
                    ref: "Order"
                }
            ]
        },
        recoveryToken : { type: String , default :'' },

        balanceRecord: [{
            type: Schema.Types.ObjectId,
            ref: 'Balance' // we need required  schema
        }],
        refundSettlement: [
            {
                type: Schema.Types.ObjectId,
                ref: 'Refund' // we need required  schema
            }
        ],
    },

    profile:{
        adminProfile: {
            type: Schema.Types.ObjectId,
            ref: 'Admin' // we need required  schema
        },
        superAdminProfile: {
            type: Schema.Types.ObjectId,
            ref: 'SuperAdmin' // we need required  schema
        },
    },

    other:{
        address:[
            {
                type: Schema.Types.ObjectId,
                ref: 'Address' // we need required  schema
            }
        ],
        ePoint:Number,
    },
        
},{
    timestamps:true
})

    userSchema.pre('save', function(next){
        var user = this;
            if(this.isModified('password') || this.isNew){
                bcrypt.genSalt(10, function(err, salt){
                    if(err){
                        return next(err)
                    }
                    bcrypt.hash(user.password, salt , function(err,hash){
                        if(err){
                            return next(err)
                        }
                        if(hash){
                            user.password = hash
                        }
                        next()
                    })
                })
            }else{
                next()
            }
}) 

module.exports = model('User', userSchema)
