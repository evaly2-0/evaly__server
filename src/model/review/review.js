const mongoose = require('mongoose')
const Schema = mongoose.Schema

const reviewSchema = new Schema ({
    shopInfo: {
        type: mongoose.Types.ObjectId,
        ref: "Shop",
        required: true
    },
    reviewInfo: {
        userInfo: {
            type: mongoose.Types.ObjectId,
            ref: "User"
        },
        review: String,
        attachment: String,
        point: {
            type: Number,
            max: 5
        }
    },
    others : {
        orderInfo: {
            type: mongoose.Types.ObjectId,
            ref: "Order"
        },
        isDeleted: {
            type: Boolean,
            default: false
        }
    }
}, {
    timestamps: true
})

//export part o
module.exports = mongoose.model ("Review", reviewSchema)