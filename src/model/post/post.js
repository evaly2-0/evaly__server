const mongoose = require('mongoose')
const Schema = mongoose.Schema

const postSchema = new Schema ({
    slug: String,
    postDetails: {
        post: {
            type: String,
            required: true
        },
        attachment: String,
        reaction: [
            {
                type: mongoose.Types.ObjectId,
                ref: "User"
            }
        ],
        comments: [
            {
                type: mongoose.Types.ObjectId,
                ref: "Comment"
            }
        ]
    }, //store post info
    postType: {
        type: String,
        enum: ["self", "announcement", "dailyNews"],
        default: "self"
    },
    others: {
        userInfo: {
            type: mongoose.Types.ObjectId,
            ref: "User",
            required: true
        },
        isDeleted: {
            type: Boolean,
            default: false
        },
        isActive: {
            type: Boolean,
            default: true
        }
    }
},{
    timestamps: true
})

//export part 
module.exports = mongoose.model ("Post", postSchema)