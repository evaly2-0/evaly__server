const mongoose = require('mongoose')
const Schema = mongoose.Schema

const appointmentSchema = new Schema({
    slug: String,
    appointmentDate: {
        type: Date,
        default: Date.now,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    appointmentTime: {
        start: {
            type: Date,
            default: Date.now,
            required: true
        },
        end : {
            type: Date,
            default: Date.now,
            required: true
        }
    },
    requestedUser: {
        type: mongoose.Types.ObjectId,
        ref: "User",
        required: true
    },
    others: {
        isDeleted: {
            type: Boolean,
            default: false
        },
        isActive: {
            type: Boolean,
            default: true
        }
    }
},{
    timestamps: true
})

//export part
module.exports = mongoose.model ("Appointment", appointmentSchema)
