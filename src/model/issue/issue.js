const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const issueSchema = new Schema({
    slug: String,
    issueType: String,
    answerOne: {
        type:String,
        // createdAt:Date.now
    },
    answerTwo: {
        type: String,
        // createdAt: Date.now
    },
    description: {
        type: String,
        // createdAt: Date.now
    },
    attachment: {
        type: String,
        // createdAt: Date.now
    },
    order: {
        type: Schema.Types.ObjectId,
        ref: 'Order'
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    isSolved: {
        type: Boolean,
        default: false
    }
}, { timestamps: true });

module.exports=mongoose.model('Issue',issueSchema);
