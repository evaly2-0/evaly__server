const {Schema, model}= require('mongoose')

const officialSchema = new Schema({
    contactNumber:String,
    contactEmail:String,
    companyLogo:String,


    appoint:{
        appointmentCategory:String,
        appointmentTimeSlot:String,
    },

    menubarCategory:[{
        type: Schema.Types.ObjectId,
        ref: 'Category', // we need required  schema
    }],

    epoint:[{
        epointCategoryName:String,
        pointRangeStart:String,
        pointRangeEnd:String,
        description:String,
    }],

    setShopReviewPoint: [
        {
            pointamount:Number,
            setThePointAccordingToTheShopCategory: String,
        }
    ],

    officialDetails:{
        totalShop:Number,
        totalIncome:Number,
        totalSell: [
            {
                type: Schema.Types.ObjectId,
<<<<<<< HEAD
                ref: "OfficialSell"
=======
                ref: 'Order', // we need required  schema
>>>>>>> master
            }
        ]
    }
        
},{
    timestamps:true
})
 
module.exports = model('Official', officialSchema)
