const {Schema, model}= require('mongoose')

const officialSellSchema = new Schema({
    month: String,
    order: [
        {
            type: Schema.Types.ObjectId,
            ref: "Order"
        }
    ],
    totalIncome: Number,
    year: String
})

module.exports = model('OfficialSell', officialSellSchema)