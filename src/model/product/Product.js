const {Schema, model}= require('mongoose')

const productSchema = new Schema({
    slug: String,
    productDetails :{
        name:{ 
            type:String, 
            required:true, 
        },
        code:{ 
            type:String, 
            required:true
        },
        model:{ 
            type:String, 
            required:true,
        },
        brand:{ 
            type: String,
            required:true,
        },
        displayImage:{ 
            type:String, 
        },
        sampleImage:[String],
        color:[{ 
            type:String, required:true,
        }],
        productSize:[{ 
            type:String, required:true,
        }],
        description:{ 
            type:String
        },
    },
    stockInfo:{
        isStock: {
            type: Boolean,
            default: false
        },
        numberOfStock: {
            type: Number,
            default: 0
        },
        color: [
            {
                colorName: String,
                stockAvailable: Number
            }
        ],
    },
    price: {
        previousPrice: {
            type: Number,
            default: 0
        },
        currentPrice: {
            type: Number
        }
    },
    othersDetails: {
        freeShipping : {
            type: Boolean,
            default: false
        },
        category:String,
        subCategory: [String],
        tags: [String],
 
        isDeleted: {
            type: Boolean,
            default: false
        }
    },
    shop :
        { 
            type: Schema.Types.ObjectId,
            ref: 'Shop' // we need required shop schema
        },
},
{
    timestamps:true
})

module.exports = model('Product', productSchema)
