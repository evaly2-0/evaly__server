const mongoose = require('mongoose');

const Schema = mongoose.Schema

const shopIncomeSchema = new Schema ({
    month: String,
    allOrder: [
        {
            income: Number,
            order: {
                type: Schema.Types.ObjectId,
                ref: "Order" 
            }
        }
    ],
    totalIncome: Number,
    year: String,
    shop: {
        type: Schema.Types.ObjectId,
        ref: "Shop"
    }
})

module.exports = mongoose.model("ShopIncome", shopIncomeSchema)