
const Campaign = require('../model/campaign/Campaign')
const catchAssyncErrors = require('../../middleware/catchAssyncErrors')
const ErrorHandeler = require('../../utils/errorHandeler')

const allcampaignGetController = catchAssyncErrors( async (req,res, next)=>{

        const campaign = await Campaign.find()
        if(campaign.length){
            res.status(200).json({
                result: campaign 
            })
        }else{
            return next(new ErrorHandeler("No campaign yet", 404));
        }

})

// get one campaign by id
const campaignGetController = catchAssyncErrors( async (req,res)=>{  

        const campaign = await Campaign.find(req.query)
        if(campaign.length){
            res.status(200).json({
                result: campaign 
            })
        }else{
            return next(new ErrorHandeler("No campaign yet", 404));
        }
})

// post an campaign 
const campaignPostController = catchAssyncErrors( async (req,res)=>{ 
        const campaign = new Campaign( {
            ...req.body,
            }
            )

        await campaign.save();
            res.status(201).json({
                message: 'campaign added successfully',
            })
})

//update campaign
const updatecampaignController = catchAssyncErrors( async (req ,res)=>{ 
        const data = await Campaign.findByIdAndUpdate(
            {_id:req.params.id},
            { $set: req.body }
            )
            res.status(200).json({
                result : data,
                message: 'campaign updated successfully'
            })
})

//delete temporary

const campaignDeleteController = catchAssyncErrors( async (req ,res)=>{  
        const data = await Campaign.findByIdAndUpdate(
            {_id:req.params.id},
            { $set:{
                isDeleted : true
                }
            }
            )
            res.status(200).json({
                result : data,
                message: 'campaign Deleted',
            })
})

//delete permanent
const campaignDeletePermanentController =catchAssyncErrors( async (req ,res)=>{  
        const data = await Campaign.findByIdAndRemove(  // findByIdAndRemove
            {_id:req.params.id},
            )
            res.status(200).json({
                result : data,
                message: 'campaign Deleted permanent',
            })
            // campaign.deleteOne({_id:req.params.id}, function (err) {
            //     if (err) return handleError(err);
            //   });

})

module.exports = {
    allcampaignGetController,
    campaignGetController,
    campaignPostController,
    updatecampaignController,
    campaignDeleteController,
    campaignDeletePermanentController
}
