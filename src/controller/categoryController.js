
const Category = require('../model/category/Category')
const catchAssyncErrors = require('../../middleware/catchAssyncErrors')
const ErrorHandeler = require('../../utils/errorHandeler')

const allcategoryGetController = catchAssyncErrors( async (req,res, next)=>{

        const category = await Category.find()
        if(category.length){
            res.status(200).json({
                result: category 
            })
        }else{
            return next(new ErrorHandeler("No category yet", 404));
        }

})

// get one category by id
const categoryGetController = catchAssyncErrors( async (req,res)=>{  

        const category = await Category.find(req.query)
        if(category.length){
            res.status(200).json({
                result: category 
            })
        }else{
            return next(new ErrorHandeler("No category yet", 404));
        }
})

// post an category 
const categoryPostController = catchAssyncErrors( async (req,res)=>{ 
        const category = new Category( {
            ...req.body,
            }
            )

        await category.save();
            res.status(201).json({
                message: 'category added successfully',
            })
})

//update category
const updatecategoryController = catchAssyncErrors( async (req ,res)=>{ 
        const data = await Category.findByIdAndUpdate(
            {_id:req.params.id},
            { $set: req.body }
            )
            res.status(200).json({
                result : data,
                message: 'category updated successfully'
            })
})

//delete temporary

const categoryDeleteController = catchAssyncErrors( async (req ,res)=>{  
        const data = await Category.findByIdAndUpdate(
            {_id:req.params.id},
            { $set:{
                isDeleted : true
                }
            }
            )
            res.status(200).json({
                result : data,
                message: 'category Deleted',
            })
})

//delete permanent
const categoryDeletePermanentController =catchAssyncErrors( async (req ,res)=>{  
        const data = await Category.findByIdAndRemove(  // findByIdAndRemove
            {_id:req.params.id},
            )
            res.status(200).json({
                result : data,
                message: 'category Deleted permanent',
            })
            // category.deleteOne({_id:req.params.id}, function (err) {
            //     if (err) return handleError(err);
            //   });

})

module.exports = {
    allcategoryGetController,
    categoryGetController,
    categoryPostController,
    updatecategoryController,
    categoryDeleteController,
    categoryDeletePermanentController
}
