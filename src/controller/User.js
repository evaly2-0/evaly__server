const tryCatch = require("../../middleware/catchAssyncErrors");
const User = require("../model/user/User");
const sendToken = require("../../utils/JwtToken.js");
exports.registerUser = tryCatch(async (req, res) => {
    const user = await User.create(req.body);

    // login when a new user is registered
    sendToken(user, 201, res);
});
