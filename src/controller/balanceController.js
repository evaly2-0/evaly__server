
const Balance = require('../model/balance/balance')
const catchAssyncErrors = require('../../middleware/catchAssyncErrors')


// get balance by id
const balanceGetController = catchAssyncErrors( async (req,res)=>{  

        const balance = await Balance.find(req.query)
        if(balance.length){
            res.status(200).json({
                result: balance 
            })
        }else{
            res.status(200).json({
                message: 'No balance'
            })
        }
})

// post an balance 
const balancePostController = catchAssyncErrors( async (req,res)=>{ 
        const balance = new Balance( {
            ...req.body,
            }
            )

        await balance.save();
            res.status(201).json({
                message: 'balance added successfully',
            })
})

//update balance
const updateBalanceController = catchAssyncErrors( async (req ,res)=>{ 
        const data = await Balance.findByIdAndUpdate(
            {_id:req.params.id},
            { $set: req.body }
            )
            res.status(200).json({
                result : data,
                message: ' Balance updated successfully',
            })
})

//delete temporary for freez balance

const balanceDeleteController = catchAssyncErrors( async (req ,res)=>{  
        const data = await Balance.findByIdAndUpdate(
            {_id:req.params.id},
            { $set:{
                isDeleted : true
                }
            }
            )
            res.status(200).json({
                result : data,
                message: ' Balance deleted successfully',
            })
})

//delete permanent
const balanceDeletePermanentController = catchAssyncErrors( async (req ,res)=>{  
        const data = await Balance.findByIdAndRemove(  // findByIdAndRemove
            {_id:req.params.id},
            )
            res.status(200).json({
                result : data,
                message: ' Balance delete permanent',
            })
            // Balance.deleteOne({_id:req.params.id}, function (err) {
            //     if (err) return handleError(err);
            //   });

})

module.exports = {
    balanceGetController,
    balancePostController,
    updateBalanceController,
    balanceDeleteController,
    balanceDeletePermanentController
}
