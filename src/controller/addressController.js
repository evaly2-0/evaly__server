
const Address = require('../model/address/Address')
const catchAssyncErrors = require('../../middleware/catchAssyncErrors')
const ErrorHandeler = require('../../utils/errorHandeler')

const allAddressGetController = catchAssyncErrors( async (req,res, next)=>{

        const address = await Address.find()
        if(address.length){
            res.status(200).json({
                result: address 
            })
        }else{
            return next(new ErrorHandeler("No address yet", 404));
        }

})

// get one address by id
const addressGetController = catchAssyncErrors( async (req,res)=>{  

        const address = await Address.find(req.query)
        if(address.length){
            res.status(200).json({
                result: address 
            })
        }else{
            return next(new ErrorHandeler("No address yet", 404));
        }
})

// post an address 
const addressPostController = catchAssyncErrors( async (req,res)=>{ 
        const address = new Address( {
            ...req.body,
            }
            )

        await address.save();
            res.status(201).json({
                message: 'address added successfully',
            })
})

//update address
const updateAddressController = catchAssyncErrors( async (req ,res)=>{ 
        const data = await Address.findByIdAndUpdate(
            {_id:req.params.id},
            { $set: req.body }
            )
            res.status(200).json({
                result : data,
                message: 'address updated successfully'
            })
})

//delete temporary

const addressDeleteController = catchAssyncErrors( async (req ,res)=>{  
        const data = await Address.findByIdAndUpdate(
            {_id:req.params.id},
            { $set:{
                isDeleted : true
                }
            }
            )
            res.status(200).json({
                result : data,
                message: 'address Deleted',
            })
})

//delete permanent
const addressDeletePermanentController =catchAssyncErrors( async (req ,res)=>{  
        const data = await Address.findByIdAndRemove(  // findByIdAndRemove
            {_id:req.params.id},
            )
            res.status(200).json({
                result : data,
                message: 'address Deleted permanent',
            })
            // Address.deleteOne({_id:req.params.id}, function (err) {
            //     if (err) return handleError(err);
            //   });

})

module.exports = {
    allAddressGetController,
    addressGetController,
    addressPostController,
    updateAddressController,
    addressDeleteController,
    addressDeletePermanentController
}
