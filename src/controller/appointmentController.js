
const Appointment = require('../model/appointment/Appointment')
const catchAssyncErrors = require('../../middleware/catchAssyncErrors')

const allAppointmentGetController =  catchAssyncErrors(  async (req,res)=>{

        const appointment = await Appointment.find()
        if(appointment.length){
            res.status(200).json({
                result: appointment 
            })
        }else{
            res.status(200).json({
                message: 'No appointment yet'
            })
        }

})

// get by id
const appointmentGetController = catchAssyncErrors( async (req,res)=>{  
        const appointment = await Appointment.find(req.query)
        if(appointment.length){
            res.status(200).json({
                result: appointment 
            })
        }else{
            res.status(200).json({
                message: 'No appointment yet'
            })
        }
})

// post a appointment
const appointmentPostController = catchAssyncErrors(async (req,res)=>{ 
        const appointment = new Appointment( {
            ...req.body,
            }
            )

        await appointment.save();
            res.status(201).json({
                message: 'appointment added successfully',
            })
})

// update a apppointment
const updateappointmentController =catchAssyncErrors(  async (req ,res)=>{  //delete temporary
        const data = await Appointment.findByIdAndUpdate(
            {_id:req.params.id},
            { $set: req.body }
            )
            res.status(200).json({
                result : data,
                message: ' Appointment updated successfully',
            })
})
const appointmentDeleteController = catchAssyncErrors( async (req ,res)=>{  //delete temporary
        const data = await Appointment.findByIdAndUpdate(
            {_id:req.params.id},
            { $set:{
                isDeleted : true
                }
            }
            )
            res.status(200).json({
                result : data,
                message: ' Appointment updated successfully',
            })

})

//delete permanent
const appointmentDeletePermanentController = catchAssyncErrors( async (req ,res)=>{  
        const data = await Appointment.findByIdAndRemove(  // findByIdAndRemove
            {_id:req.params.id},
            )
            res.status(200).json({
                result : data,
                message: ' Appointment deleted successfully',
            })
            // Appointment.deleteOne({_id:req.params.id}, function (err) {
            //     if (err) return handleError(err);
            //   });
})

module.exports = {
    allAppointmentGetController,
    appointmentGetController,
    appointmentPostController,
    updateappointmentController,
    appointmentDeleteController,
    appointmentDeletePermanentController
}
