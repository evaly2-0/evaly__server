
const Cart = require('../model/cart/Cart')
const catchAssyncErrors = require('../../middleware/catchAssyncErrors')
const ErrorHandeler = require('../../utils/errorHandeler')

const allcartGetController = catchAssyncErrors( async (req,res, next)=>{

        const cart = await Cart.find()
        if(cart.length){
            res.status(200).json({
                result: cart 
            })
        }else{
            return next(new ErrorHandeler("No cart yet", 404));
        }

})

// get one cart by id
const cartGetController = catchAssyncErrors( async (req,res)=>{  

        const cart = await Cart.find(req.query)
        if(cart.length){
            res.status(200).json({
                result: cart 
            })
        }else{
            return next(new ErrorHandeler("No cart yet", 404));
        }
})

// post an cart 
const cartPostController = catchAssyncErrors( async (req,res)=>{ 
        const cart = new Cart( {
            ...req.body,
            }
            )

        await cart.save();
            res.status(201).json({
                message: 'cart added successfully',
            })
})

//update cart
const updatecartController = catchAssyncErrors( async (req ,res)=>{ 
        const data = await Cart.findByIdAndUpdate(
            {_id:req.params.id},
            { $set: req.body }
            )
            res.status(200).json({
                result : data,
                message: 'cart updated successfully'
            })
})

//delete temporary

const cartDeleteController = catchAssyncErrors( async (req ,res)=>{  
        const data = await Cart.findByIdAndUpdate(
            {_id:req.params.id},
            { $set:{
                isDeleted : true
                }
            }
            )
            res.status(200).json({
                result : data,
                message: 'cart Deleted',
            })
})

//delete permanent
const cartDeletePermanentController =catchAssyncErrors( async (req ,res)=>{  
        const data = await Cart.findByIdAndRemove(  // findByIdAndRemove
            {_id:req.params.id},
            )
            res.status(200).json({
                result : data,
                message: 'cart Deleted permanent',
            })
            // cart.deleteOne({_id:req.params.id}, function (err) {
            //     if (err) return handleError(err);
            //   });

})

module.exports = {
    allcartGetController,
    cartGetController,
    cartPostController,
    updatecartController,
    cartDeleteController,
    cartDeletePermanentController
}
