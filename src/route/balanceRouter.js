
const router = require ('express').Router()
// const Auth = require('../../middleware/auth')
// const { permission } = require('../../middleware/permission')

const { 
    balanceGetController,
    balancePostController,
    updateBalanceController,
    balanceDeleteController,
    balanceDeletePermanentController

    } = require('../controller/balanceController')

    router.get('/viewbalance/:id', balanceGetController)
    router.post('/postbalance' , balancePostController) // permission(['admin']),
    router.put('/updatebalance/:id', updateBalanceController) // ,permission(['admin'])
    router.delete('/deletebalance/:id', balanceDeleteController) // temporary // ,permission(['admin'])
    router.delete('/deletebalancePermanent/:id', balanceDeletePermanentController) // ,permission(['admin'])
 

module.exports =router
