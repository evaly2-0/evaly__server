
const router = require ('express').Router()
// const Auth = require('../../middleware/auth')
// const { permission } = require('../../middleware/permission')

const { 
    allcategoryGetController,
    categoryGetController,
    categoryPostController,
    updatecategoryController,
    categoryDeleteController,
    categoryDeletePermanentController

    } = require('../controller/categoryController')

    router.get('/', allcategoryGetController)
    router.get('/viewOnecategory/:id', categoryGetController)
    router.post('/postcategory' , categoryPostController) // permission(['admin']),
    router.put('/updatecategory/:id', updatecategoryController) // ,permission(['admin'])
    router.delete('/deletecategory/:id', categoryDeleteController) // temporary ,permission(['admin'])
    router.delete('/deletecategoryPermanent/:id', categoryDeletePermanentController) // ,permission(['admin'])
 

module.exports =router

// router.post('/postAddress',fileuploader.single('image') , admitSingleStudentController) //fileUploader.fields([{'bookimage'}]), problem,,  permission(['admin']),
 