
const router = require ('express').Router()
// const Auth = require('../../middleware/auth')
// const { permission } = require('../../middleware/permission')

const { 
    allcampaignGetController,
    campaignGetController,
    campaignPostController,
    updatecampaignController,
    campaignDeleteController,
    campaignDeletePermanentController

    } = require('../controller/campaignController')

    router.get('/', allcampaignGetController)
    router.get('/viewOnecampaign/:id', campaignGetController)
    router.post('/postcampaign' , campaignPostController) // permission(['admin']),
    router.put('/updatecampaign/:id', updatecampaignController) // ,permission(['admin'])
    router.delete('/deletecampaign/:id', campaignDeleteController) // temporary ,permission(['admin'])
    router.delete('/deletecampaignPermanent/:id', campaignDeletePermanentController) // ,permission(['admin'])
 

module.exports =router
