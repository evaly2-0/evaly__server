
const router = require ('express').Router()
// const Auth = require('../../middleware/auth')
// const { permission } = require('../../middleware/permission')

const { 
    allcartGetController,
    cartGetController,
    cartPostController,
    updatecartController,
    cartDeleteController,
    cartDeletePermanentController

    } = require('../controller/cartController')

    router.get('/', allcartGetController)
    router.get('/viewOnecart/:id', cartGetController)
    router.post('/postcart' , cartPostController) // permission(['admin']),
    router.put('/updatecart/:id', updatecartController) // ,permission(['admin'])
    router.delete('/deletecart/:id', cartDeleteController) // temporary ,permission(['admin'])
    router.delete('/deletecartPermanent/:id', cartDeletePermanentController) // ,permission(['admin'])
 

module.exports =router

// router.post('/postcart',fileuploader.single('image') , admitSingleStudentController) //fileUploader.fields([{'bookimage'}]), problem,,  permission(['admin']),
 