
const router = require ('express').Router()
// const Auth = require('../../middleware/auth')
// const { permission } = require('../../middleware/permission')

const { 
    allAppointmentGetController,
    appointmentGetController,
    appointmentPostController,
    updateappointmentController,
    appointmentDeleteController,
    appointmentDeletePermanentController

    } = require('../controller/appointmentController')

    router.get('/', allAppointmentGetController)
    router.get('/viewOneAppointment/:id', appointmentGetController)
    router.post('/postAppointment' , appointmentPostController) // permission(['admin']),
    router.put('/updateAppointment/:id', updateappointmentController) // ,permission(['admin'])
    router.delete('/deleteAppointment/:id', appointmentDeleteController) // ,permission(['admin'])// temporary
    router.delete('/deleteAppointmentPermanent/:id', appointmentDeletePermanentController)// ,permission(['admin']) // temporary
 

module.exports =router

// router.post('/postAddress',fileuploader.single('image') , admitSingleStudentController) //fileUploader.fields([{'bookimage'}]), problem,,  permission(['admin']),
 