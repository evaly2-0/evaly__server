
const router = require ('express').Router()
// const Auth = require('../../middleware/auth')
// const { permission } = require('../../middleware/permission')

const { 
    allAddressGetController,
    addressGetController,
    addressPostController,
    updateAddressController,
    addressDeleteController,
    addressDeletePermanentController

    } = require('../controller/addressController')

    router.get('/', allAddressGetController)
    router.get('/viewOneAddress/:id', addressGetController)
    router.post('/postAddress' , addressPostController) // permission(['admin']),
    router.put('/updateAddress/:id', updateAddressController) // ,permission(['admin'])
    router.delete('/deleteAddress/:id', addressDeleteController) // temporary ,permission(['admin'])
    router.delete('/deleteAddressPermanent/:id', addressDeletePermanentController) // ,permission(['admin'])
 

module.exports =router

// router.post('/postAddress',fileuploader.single('image') , admitSingleStudentController) //fileUploader.fields([{'bookimage'}]), problem,,  permission(['admin']),
 