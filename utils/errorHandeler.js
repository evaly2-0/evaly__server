class ErrorHandeler extends Error {
    constructor(message, statusCode) {
        super(message);
        this.statusCode = statusCode;
        Error.captureStackTrace(this, this.constructor);
    }
}

module.exports = ErrorHandeler;

// const user = await User.findOne({ email });
// if (!user) {
//     return next(new ErrorHandeler("Invalid Email or Password", 401));
// }
